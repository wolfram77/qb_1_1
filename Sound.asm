.MODEL MEDIUM, BASIC

.STACK 100H


.DATA

ManagerState	db 0
Buffer 		db 0
TimeConstant	db 0
Lengths		db 0,0
DMAChannel	db 0
DMAChannelMask	db 0
DMAModeData	db 0
DMAPage		db 0
DMAAddress	db 0,0
DMAPages	db 0,0
DMAAddresses	db 0,0,0,0
OldHandler	dd 0

.386
.CODE


;-------------------------------------------------------------------------------------
;		SOUND MANAGER			
; Use: Internal Handler
; ManagerState: 	b0 = Control (On/Off)
;			b1 = Programmer Acknowledge
;------------------------------------------------------------------------------------

SoundManager:
;Test Sound Manager State
TEST ManagerState, 1
JNZ SMStart
PUSH AX
PUSH DX
MOV DX, 22Eh
IN AL, DX
MOV AL, 20h
OUT 20h, AL
POP DX
POP AX
IRET

;Start the sound first
SMStart:
stI
PUSH AX
PUSH BX
PUSH DX
MOV DX, 22Eh
IN AL, DX
MOV AL, DMAChannelMask
OUT 0Ah, AL
XOR AL, AL
OUT 0Ch, AL
MOV AL, DMAModeData
OUT 0Bh, AL
MOV AL, DMAPage
OUT 83h, AL
MOV AL, DMAAddress[0]
OUT 2h, AL
MOV AL, DMAAddress[1]
OUT 2h, AL
MOV AL, Lengths[0]
OUT 3h, AL
MOV AL, Lengths[1]
OUT 3h, AL
MOV AL, DMAChannel
OUT 0Ah, AL
MOV DX, 22Ch
MOV BL, 40h
CALL WrtDSP
MOV BL, TimeConstant
CALL WrtDSP
MOV BL, 14h
CALL WrtDSP
MOV BL, Lengths[0]
CALL WrtDSP
MOV BL, Lengths[1]
CALL WrtDSP

;Now, prepare for next action.
INC Buffer
TEST Buffer,1
JNZ SMBuffer2
;SMBuffer1
MOV AL, DMAPages[0]
MOV DMAPage, AL
MOV AL, DMAAddresses[0]
MOV DMAAddress[0], AL
MOV AL, DMAAddresses[1]
MOV DMAAddress[1], AL
JMP SMBuffers
SMBuffer2:
MOV AL, DMAPages[1]
MOV DMAPage, AL
MOV AL, DMAAddresses[2]
MOV DMAAddress[0], AL
MOV AL, DMAAddresses[3]
MOV DMAAddress[1], AL
SMBuffers:

;Now, tell the programmer
OR ManagerState, 2
;Exit
MOV AL, 20h
OUT 20h, AL
POP DX
POP BX
POP AX
IRET

WrtDSP:
; Write to DSP from the interrupt handler.
; Input: BL = Data
;
IN AL, DX
OR AL, AL
JS WrtDSP
MOV AL, BL
OUT DX, AL
RET


;---------------------------------------------------------------------------
;		SOUND MANAGER			
; Use: Control Routines
;---------------------------------------------------------------------------

; SUB SndInst (  )
; Use: Install Sound manager
EVEN
PUBLIC SndInst
SndInst PROC
XOR AX, AX
MOV ES, AX
MOV EAX, ES:[52]
MOV OldHandler, EAX
MOV AX, SEG SoundManager
SHL EAX,16
MOV AX, OFFSET SoundManager
MOV ES:[52], EAX
RETF
SndInst ENDP

; SUB SndUninst (  )
; Use: Uninstall Sound manager
EVEN
PUBLIC SndUninst
SndUninst PROC
XOR AX, AX
MOV ES, AX
MOV EAX, OldHandler
MOV ES:[52], EAX
RETF
SndUninst ENDP

; SUB SndControl ( Control (On/Off) )
; Use: Switch Sound Manager on or off.
EVEN
PUBLIC SndControl
SndControl PROC
PUSH BP
MOV BP, SP
MOV AX, [BP+6]
AND ManagerState, 0FEh
CMP AX, 0
JBE SMControl
OR ManagerState, 1
SMControl:
POP BP
RETF 2
SndControl ENDP

; FUNCTION SndOver ( )
; Use: See if a sample is over.
EVEN
PUBLIC SndOver
SndOver PROC
XOR AX, AX
MOV AL, ManagerState
SHR AL, 1
AND ManagerState, 0FDh
RETF
SndOver ENDP

; SUB SndLength ( Length )
; Use: Set the Lengths of sound sent.
EVEN
PUBLIC SndLength
SndLength PROC
PUSH BP
MOV BP, SP
MOV AX, [BP+6]
DEC AX
MOV Lengths[0], AL
MOV Lengths[1], AH
POP BP
RETF 4
SndLength ENDP

; SUB SndInf ( DMAChannel, ModeData, Seg1, Seg2 )
; Use: Set the sound Information
EVEN
PUBLIC SndInf
SndInf PROC
PUSH BP
MOV BP, SP
MOV AX, [BP+8]
SHR AX, 12
MOV DMAPages[0], AL
MOV DMAPage, AL
MOV AX, [BP+6]
SHR AX,12
MOV DMAPages[1], AL
MOV AX, [BP+8]
SHL AX, 4
MOV WORD PTR DMAAddresses[0], AX
MOV WORD PTR DMAAddress[0], AX
MOV AX, [BP+6]
SHL AX, 4
MOV WORD PTR DMAAddresses[2], AX
MOV AL, [BP+10]
MOV DMAModeData, AL
MOV AL, [BP+12]
MOV DMAChannel, AL
OR AL, 4
MOV  DMAChannelMask, AL
POP BP
RETF 8
SndInf ENDP

; SUB SndTime ( TimeConstant )
; Use: Set the sound constant
EVEN
PUBLIC SndTime
SndTime PROC
PUSH BP
MOV BP, SP
MOV AL, [BP+6]
MOV TimeConstant, AL
POP BP
RETF 2
SndTime ENDP


; SUB SndStart (  )
; Use: Start the sound.
EVEN
PUBLIC SndStart
SndStart PROC
PUSHF
PUSH CS
CALL SoundManager
RETF
SndStart ENDP

; SUB SndWriteDSP (  )
; Use: Start the sound.


END