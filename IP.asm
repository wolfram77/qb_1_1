The IP Addresses are 32-bit numbers.
B1.B2.B3.B4

Class A Network
------------------------
Used for very large networks (ex- Arpanet, etc.).
B1 = 1-126 : Network number
B2.B3.B4 = Host number

Class B Network
------------------------
Used for normal large organisations.
B1.B2 = 129.1 - 191.254 (0,255 avoided) (127.x is used for special purposes) : Network number
B3.B4 = Host number (64516 computers)
(It is possible to get more than one class B address if one runs out.)

Class C Network
-----------------------
B1.B2.B3 = 192.1.1 - 223.254.254 : Network number (there can be lots of these networks)
B4 = Host number (254 computers)
(Addresses above 223 are reserved as class D and E(currently not defined.)


Subnet is used in Class B networks where there is internal network division but not any external
division.

0 is reserved for machines which don't know their address.
0.0.0.23 = Unknown network; Host = 23

255  = Broadcast
128.6.4.255 : 128.6.4 = Network address; 255=broadcast to ethernet
255.255.255.255 = All hosts on local network