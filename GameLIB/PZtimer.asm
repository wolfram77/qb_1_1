
.MODEL MEDIUM, BASIC

BASE_8253	equ	40h
TIMER_0_8253     equ	BASE_8253 + 0
MODE_8253        equ	BASE_8253 + 3
OCW3              equ	20h
IRR               equ	20h

MPOPF macro
      local p1, p2
      jmp short p2
p1:   iret             ; jump to pushed address & pop flags
p2:   push cs          ; construct far return address to
      call p1          ; the next instruction
      endm

DELAY macro
      jmp     $+2
      jmp     $+2
      jmp     $+2
      endm

.STACK 200H


.DATA

OriginalFlags   db    ?
TimedCount      dw    ?
ReferenceCount  dw ?
OverflowFlag    db    ?
OutputStr  db  0dh, 0ah, 'Timed count: ', 5 dup (?)
ASCIICountEnd    db    ' microseconds', 0dh, 0ah
           db    '$'
OverflowStr       db    0dh, 0ah
      db    '****************************************************'
      db    0dh, 0ah
      db    '* The timer overflowed, so the interval timed was  *'
      db    0dh, 0ah
      db    '* too long for the precision timer to measure.     *'
      db    0dh, 0ah
      db    '* Please perform the timing test again with the    *'
      db   0dh, 0ah
      db    '* long-period timer.                               *'
      db    0dh, 0ah
      db    '****************************************************'
      db    0dh, 0ah
      db    '$'

; ********************************************************************
; * Routine called to start timing.                                  *
; ********************************************************************

.386
.CODE
ZTimerOn    proc
   push   ax
   pushf
   pop    ax                       ; get flags so we can keep
   mov    cs:[OriginalFlags],ah    ; remember the state of the
   and    ah,0fdh                  ; set pushed interrupt flag
   push   ax
     sti
     mov  al,00110100b               ;mode 2
     out  MODE_8253,al
     DELAY
     sub     al,al
     out     TIMER_0_8253,al     ;lsb
     DELAY
     out     TIMER_0_8253,al     ;msb
    rept 10
    jmp   $+2
    endm
     cli
      mov  al,00110100b        ; set up to load initial
      out  MODE_8253,al        ; timer count
      DELAY
      sub  al,al
      out  TIMER_0_8253,al     ; load count lsb
      DELAY
      out  TIMER_0_8253,al; load count msb
     MPOPF                   ; keeps interrupts off
     pop   ax
     ret

ZTimerOn     endp

;********************************************************************
;* Routine called to stop timing and get count.                     *
;********************************************************************

ZTimerOff proc
     push    ax
     push    cx
     pushf
     mov  al,00000000b     ; latch timer 0
     out  MODE_8253,al
     mov   al,00001010b        ; OCW3, set up to read
     out   OCW3,al; Int        errupt Request register
     DELAY
     in al,IRR; read         Interrupt Request
     and   al,1                ; set AL to 1 if IRQ0 (the
     mov   cs:[OverflowFlag],al; store the timer overflow
      sti
     in     al,TIMER_0_8253   ; least significant byte
     DELAY
     mov    ah,al
     in     al,TIMER_0_8253   ; most significant byte
     xchg   ah,al
     neg    ax                ; convert from countdown
     mov    cs:[TimedCount],ax
     mov   cs:[ReferenceCount],0
     mov   cx,16
     cli                ; interrupts off to allow a
 RefLoop:
     call   ReferenceZTimerOn
     call   ReferenceZTimerOff
     loop   RefLoop
     sti
     add    cs:[ReferenceCount],8; total + (0.5 * 16)
     mov    cl,4
     shr    cs:[ReferenceCount],cl; (total) / 16 + 0.5
     pop    ax                    ; retrieve flags when called
     mov    ch,cs:[OriginalFlags] ; get back the original upper
     and    ch,not 0fdh           ; only care about original
     and    ah,0fdh               ; ...keep all other flags in
     or     ah,ch                 ; make flags word with original
     push   ax                    ; prepare flags to be popped
    MPOPF                      ; restore the flags with the
    pop    cx
    pop    ax
    ret

ZTimerOff  endp

ReferenceZTimerOn proc
      push  ax
      pushf     ; interrupts are already off
   mov    al,00110100b    ; set up to load
   out    MODE_8253,al    ; initial timer count
   DELAY
     sub    al,al
     out    TIMER_0_8253,al; load count lsb
     DELAY
     out    TIMER_0_8253,al; load count msb
     MPOPF
     pop    ax
     ret

ReferenceZTimerOn endp

ReferenceZTimerOff proc
      push   ax
      push   cx
      pushf
     mov   al,00000000b        ; latch timer 0
     out   MODE_8253,al
     DELAY
     in    al,TIMER_0_8253     ; lsb
     DELAY
     mov   ah,al
     in    al,TIMER_0_8253     ; msb
     xchg  ah,al
     neg   ax                  ; convert from countdown
     add   cs:[ReferenceCount],ax
    MPOPF
    pop    cx
    pop    ax
    ret

ReferenceZTimerOff endp

; ********************************************************************
; * Routine called to report timing results.                         *
; ********************************************************************

ZTimerReport proc

       pushf
       push  ax
       push  bx
       push  cx
       push  dx
       push  si
       push  ds
       push       cs     ; DOS functions require that DS point
       pop        ds     ; to text to be displayed on the screen
     cmp  [OverflowFlag],0
     jz   PrintGoodCount
     mov  dx,offset OverflowStr
     mov  ah,9
     int  21h
     jmp  short EndZTimerReport
PrintGoodCount:
     mov   ax,[TimedCount]
     sub   ax,[ReferenceCount]
     mov   si,offset ASCIICountEnd - 1
     mov   dx, 8381
     mul   dx
     mov   bx, 10000
     div   bx                ;* .8381 = * 8381 / 10000
     mov   bx, 10
     mov   cx, 5
CTSLoop:
     sub   dx, dx
     div   bx
      add  dl,'0'
     mov   [si],dl
     dec   si
     loop  CTSLoop
     mov   ah, 9
     mov   dx, offset OutputStr
     int   21h 
EndZTimerReport:
     pop   ds
     pop   si
     pop   dx
     pop   cx
     pop   bx
     pop   ax
     MPOPF
     ret

ZTimerReport  endp

end

