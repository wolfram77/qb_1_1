.MODEL MEDIUM, BASIC

.STACK 200H

.DATA

VideoScreen 		DW	?
VideoFlag		DW	0
Xlimit1			DW	0
Xlimit2			DW	319
Ylimit1			DW	0
Ylimit2			DW	199
Xdiff			DW	320
Ydiff			DW	200
FreeVar			DW	3 DUP(?)
LineModes1		DW	LineMode11, LineMode12, LineMode13, LineMode14
LineModes2		DW	offset LineMode21, offset LineMode22, offset LineMode23, offset LineMode24


.386
.CODE

; DECLARE SUB ScreenDisplay ( )
; ScreenDisplay ( )
; Waits for vertical retrace, anf then finally dashes
; all the contents of VideoBuffer to VideoScreen.
;
PUBLIC ScreenDisplay
ScreenDisplay PROC
PUSHF
PUSH DS
PUSH SI
PUSH DI
XOR SI, SI
XOR DI, DI
MOV DS, VideoScreen
MOV AX, 0A000h
MOV ES, AX
MOV CX, 16000
MOV DX, 3DAh
CLD
WaitForRetrace:
IN AL, DX
TEST AL, 8
JZ WaitForRetrace
REP MOVSD
POP DI
POP SI
POP DS
POPF
RETF
ScreenDisplay ENDP


; DECLARE SUB Dot (BYVAL Xpixel%, BYVAL Ypixel%, BYVAL Colour% )
; Dot ( Xpixel, Ypixel, Colour )
; Draws a dot (pixel).
; 
PUBLIC Dot
Dot PROC
PUSH BP
MOV BP, SP
MOV AX, [BP+10]
MOV BX, [BP+8]
TEST VideoFlag, 1
JZ DotNotRelative
ADD AX, Xlimit1
ADD BX, Ylimit1
DotNotRelative:
CMP AX, Xlimit1
JL NoDot
CMP AX, Xlimit2
JG NoDot
CMP BX, Ylimit1
JL NoDot
CMP BX, Ylimit2
JG NoDot
SHL BX , 8
MOV CX, BX
SHR CX, 2
ADD BX, CX
ADD BX, AX
MOV CL, [BP+6]
MOV ES, VideoScreen
MOV ES:[BX], CL
NoDot:
POP BP
RETF 6
Dot ENDP


; DECLARE SUB VideoBufferSeg ( BYVAL VideoSegment% )
; VideoBufferSeg ( VideoSegment )
; Sets the VideoBuffer Segment, allocated by an array.
;
PUBLIC VideoBufferSeg
VideoBufferSeg PROC
PUSH BP
MOV BP, SP
MOV AX, [BP+6]
MOV VideoScreen, AX
POP BP
RETF 2
VideoBufferSeg ENDP


; DECLARE SUB ScreenSync (  )
; VideoBufferSeg ( VideoSegment )
; Waits till Vertical retrace is over.
; Ensures no flicker when program is too fast.
;
PUBLIC ScreenSync
ScreenSync PROC
MOV DX, 3DAh
WaitTillRetrace:
IN AL, DX
TEST AL, 8
JNZ WaitTillRetrace
RETF
ScreenSync ENDP


; DECLARE SUB VideoLimit ( BYVAL Xlimit1%, BYVAL Ylimit1%, BYVAL Xlimit2%, BYVAL Ylimit2%, BYVAL Relative% )
; VideoLimit ( Xlimit1, Ylimit1, Xlimit2, Ylimit2, Relative )
; Sets the video limit in which the graphics is to be displayed.
; Also sets if graphics will be relative or not.
;
PUBLIC VideoLimit
VideoLimit PROC
PUSH BP
MOV BP, SP
MOV AX, [BP+14]
MOV BX, [BP+12]
MOV CX, [BP+10]
MOV DX, [BP+8]
MOV Xlimit1, AX
MOV Ylimit1, BX
MOV Xlimit2, CX
MOV Ylimit2, DX
SUB CX, AX
INC CX
MOV Xdiff, CX
SUB DX, BX
INC DX
MOV Ydiff, DX
MOV AX, [BP+6]
AND AX, 1
OR VideoFlag, AX
POP BP
RETF 10
VideoLimit ENDP


; DECLARE FUNCTION GetDot% ( BYVAL Xpixel%, BYVAL Ypixel% )
; GetDot ( Xpixel, Ypixel )
; Get colour of a dot.
;
PUBLIC GetDot
GetDot PROC
PUSH BP
MOV BP, SP
MOV AX, 0FFFFh
MOV CX, [BP+8]
MOV BX, [BP+6]
TEST VideoFlag, 1
JZ GetDotNotRelative
ADD CX, Xlimit1
ADD BX, Ylimit1
GetDotNotRelative:
CMP CX, Xlimit1
JL NoGetDot
CMP CX, Xlimit2
JG NoGetDot
CMP BX, Ylimit1
JL NoGetDot
CMP BX, Ylimit2
JG NoGetDot
SHL BX, 8
MOV DX, BX
SHR DX, 2
ADD BX, DX
ADD BX, CX
MOV ES, VideoScreen
MOV AX, ES:[BX]
NoGetDot:
POP BP
RETF 4
GetDot ENDP


; DECLARE SUB FillScreen ( BYVAL Colour% )
; FillScreen ( Colour )
; Get colour of a dot.
;
PUBLIC FillScreen
FillScreen PROC
PUSH BP
MOV BP, SP
MOV BX, Xdiff
MOV DX, BX
SHR BX, 2
AND DX, 3
MOV BH, DL
MOV DX, Ylimit1
SHL DX, 8
MOV AX, DX
SHR AX, 2
ADD DX, AX
ADD DX, Xlimit1
PUSH SI
PUSH DI
MOV SI, DX
MOV DX, Ydiff
MOV AL, [BP+6]
MOV AH, AL
SHL EAX, 16
MOV AL, [BP+6]
MOV AH, AL
XOR CX, CX
MOV ES, VideoScreen
PUSHF
CLD
FillScreenLoop:
MOV DI, SI
MOV CL, BL
REP STOSD
MOV CL, BH
REP STOSB
ADD SI, 320
DEC DX
JNZ FillScreenLoop
POPF
POP DI
POP SI
POP BP
RETF 2
FillScreen ENDP


; DECLARE SUB Line ( BYVAL Xpix1%, BYVAL Ypix1%, BYVAL Xpix2%, BYVAL Ypix2%, BYVAL Colour%, BYVAL Mode% )
; Line ( Xpix1, Ypix1, Xpix2, Ypix2, Colour, Mode )
; Draws a line.
; 
PUBLIC Line
Line PROC
PUSH BP
MOV BP, SP
PUSH ESI
PUSH EDI
PUSH EBP
MOV AX, [BP+16]
MOV BX, [BP+14]
MOV CX, [BP+12]
MOV DX, [BP+10]
TEST VideoFlag, 1
JZ LineNotRelative
ADD AX, Xlimit1
ADD BX, Ylimit1
ADD CX, Xlimit1
ADD DX, Ylimit1
MOV [BP+16], AX
MOV [BP+14], BX
MOV [BP+12], CX
MOV [BP+10], DX
LineNotRelative:
CMP AX, Xlimit1
JGE LineLcase1
CMP CX, Xlimit1
JL NoLine
LineLcase1:
CMP AX, Xlimit2
JLE LineLcase2
CMP CX, Xlimit2
JG NoLine
LineLcase2:
CMP BX, Ylimit1
JGE LineLcase3
CMP DX, Ylimit1
JL NoLine
LineLcase3:
CMP BX, Ylimit2
JLE LineLcase4
CMP DX, Ylimit2
JG NoLine
LineLcase4:
SUB CX, AX
SUB DX, BX
PUSH CX
PUSH DX
MOV AX, CX
CWD
MOV EBX, EAX
MOV AX, DX
CWD
MOV ECX, EAX
XOR EAX, EAX
OR EAX, EAX
JZ LineSxy0
MOV EAX, EBX
SHL EAX, 16
CDQ
IDIV ECX
LineSxy0:
MOV ESI, EAX
XOR EAX, EAX
OR EBX, EBX
JZ LineSyx0
MOV EAX, ECX
SHL EAX, 16
CDQ
IDIV EBX
LineSyx0:
MOV EDI, EAX
MOV BX, [BP+16]
CMP BX, Xlimit1
JGE LineBcase1
MOV BX, Xlimit1
SUB BX, AX
CWD
IMUL EDI
SHR EAX, 16
ADD [BP+14], AX
MOV AX, Xlimit1
MOV [BP+16], AX
LineBcase1:
CMP BX, Xlimit2
JGE LineBcase2
MOV BX, Xlimit2
SUB BX, AX
CWD
IMUL EDI
SHR EAX, 16
ADD [BP+14], AX
MOV AX, Xlimit2
MOV [BP+16], AX
LineBcase2:
MOV BX, [BP+14]
CMP BX, Ylimit1
JGE LineBcase3
MOV BX, Ylimit1
SUB BX, AX
CWD
IMUL ESI
SHR EAX, 16
ADD [BP+16], AX
MOV AX, Ylimit1
MOV [BP+14], AX
LineBcase3:
CMP BX, Ylimit2
JGE LineBcase4
MOV BX, Ylimit2
SUB BX, AX
CWD
IMUL ESI
SHR EAX, 16
ADD [BP+16], AX
MOV AX, Ylimit2
MOV [BP+14], AX
LineBcase4:
MOV BX, [BP+12]
CMP BX, Xlimit1
JGE LineBcase5
MOV BX, Xlimit1
SUB BX, AX
CWD
IMUL EDI
SHR EAX, 16
ADD [BP+10], AX
MOV AX, Xlimit1
MOV [BP+12], AX
LineBcase5:
CMP BX, Xlimit2
JGE LineBcase6
MOV BX, Xlimit2
SUB BX, AX
CWD
IMUL EDI
SHR EAX, 16
ADD [BP+10], AX
MOV AX, Xlimit2
MOV [BP+12], AX
LineBcase6:
MOV BX, [BP+10]
CMP BX, Ylimit1
JGE LineBcase7
MOV BX, Ylimit1
SUB BX, AX
CWD
IMUL ESI
SHR EAX, 16
ADD [BP+12], AX
MOV AX, Ylimit1
MOV [BP+10], AX
LineBcase7:
CMP BX, Ylimit2
JGE LineBcase8
MOV BX, Ylimit2
SUB BX, AX
CWD
IMUL ESI
SHR EAX, 16
ADD [BP+12], AX
MOV AX, Ylimit2
MOV [BP+10], AX
LineBcase8:
MOV ES, VideoScreen
MOV BX, [BP+14]
SHL BX, 8
MOV BX, AX
SHR AX, 2
ADD BX, AX
ADD BX,[BP+16]
POP DX
POP CX
MOV FreeVar[0], CX
MOV FreeVar[1], DX
MOV AX, [BP+6]
MOV FreeVar[2], AX
TEST CX, 8000h
JZ LineXve
NEG CX
LineXve:
TEST DX, 8000h
JZ LineYve
NEG DX
LineYve:
CMP CX, DX
JLE LineSlope2
LineSlope1:
MOV CL, [BP+8]
MOV AX, FreeVar[0]
CWD
MOV EBP, EAX
MOV AX, BX
MOV BX, FreeVar[2]
AND BX, 3
TEST BP, 8000h
JMP LineModes1[BX]
LineMode11:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode11Step], 4D66h
JZ LineMode11ve
MOV WORD PTR CS:[OFFSET LineMode11Step], 4566h
LineMode11ve:
MOV EAX, EBP
IMUL EDI
SHR EAX, 16
MOV SI, 320
IMUL SI
MOV SI, BX
ADD SI, AX
ADD SI, BP
MOV ES:[SI], CL
LineMode11Step:
NOP
NOP
JNZ LineMode11ve
JMP LineOver
LineMode12:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode12Step], 4D66h
JZ LineMode12ve
MOV WORD PTR CS:[OFFSET LineMode12Step], 4566h
LineMode12ve:
MOV EAX, EBP
IMUL EDI
SHR EAX, 16
MOV SI, 320
IMUL SI
MOV SI, BX
ADD SI, AX
ADD SI, BP
MOV AL, ES:[SI]
XOR AL, CL
MOV ES:[SI], AL
LineMode12Step:
NOP
NOP
JNZ LineMode12ve
JMP LineOver
LineMode13:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode13Step], 4D66h
JZ LineMode13ve
MOV WORD PTR CS:[OFFSET LineMode13Step], 4566h
LineMode13ve:
MOV EAX, EBP
IMUL EDI
SHR EAX, 16
MOV SI, 320
IMUL SI
MOV SI, BX
ADD SI, AX
ADD SI, BP
NOT BYTE PTR ES:[SI]
LineMode13Step:
NOP
NOP
JNZ LineMode13ve
JMP LineOver
LineMode14:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode14Step], 4D66h
JZ LineMode14ve
MOV WORD PTR CS:[OFFSET LineMode14Step], 4566h
LineMode14ve:
MOV EAX, EBP
IMUL EDI
SHR EAX, 16
MOV SI, 320
IMUL SI
MOV SI, BX
ADD SI, AX
ADD SI, BP
MOV ES:[SI], CL
INC CL
LineMode14Step:
NOP
NOP
JNZ LineMode14ve
JMP LineOver
LineSlope2:
MOV CL, [BP+8]
MOV AX, FreeVar[1]
CWD
MOV EBP, EAX
MOV AX, BX
MOV BX, [BP+6]
AND BX, 3
TEST BP, 8000h
JMP LineModes2[BX]
LineMode21:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode21Step], 4D66h
JZ LineMode21ve
MOV WORD PTR CS:[OFFSET LineMode21Step], 4566h
LineMode21ve:
MOV EAX, EBP
IMUL ESI
SHR EAX, 16
MOV DI, BX
ADD DI, AX
MOV AX, 320
IMUL BP
ADD DI, AX
MOV ES:[DI], CL
LineMode21Step:
NOP
NOP
JNZ LineMode21ve
JMP LineOver
LineMode22:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode22Step], 4D66h
JZ LineMode22ve
MOV WORD PTR CS:[OFFSET LineMode22Step], 4566h
LineMode22ve:
MOV EAX, EBP
IMUL ESI
SHR EAX, 16
MOV DI, BX
ADD DI, AX
MOV AX, 320
IMUL BP
ADD DI, AX
MOV AL, ES:[DI]
XOR AL, CL
MOV ES:[DI], AL
LineMode22Step:
NOP
NOP
JNZ LineMode22ve
JMP LineOver
LineMode23:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode23Step], 4D66h
JZ LineMode23ve
MOV WORD PTR CS:[OFFSET LineMode23Step], 4566h
LineMode23ve:
MOV EAX, EBP
IMUL ESI
SHR EAX, 16
MOV DI, BX
ADD DI, AX
MOV AX, 320
IMUL BP
ADD DI, AX
NOT BYTE PTR ES:[DI]
LineMode23Step:
NOP
NOP
JNZ LineMode23ve
JMP LineOver
LineMode24:
MOV BX, AX
MOV WORD PTR CS:[OFFSET LineMode24Step], 4D66h
JZ LineMode24ve
MOV WORD PTR CS:[OFFSET LineMode24Step], 4566h
LineMode24ve:
MOV EAX, EBP
IMUL ESI
SHR EAX, 16
MOV DI, BX
ADD DI, AX
MOV AX, 320
IMUL BP
ADD DI, AX
MOV ES:[DI], CL
LineMode24Step:
NOP
NOP
JNZ LineMode24ve
;JMP LineOver
LineOver:
NoLine:
POP EBP
POP EDI
POP ESI
POP BP
RETF 12
Line ENDP




END