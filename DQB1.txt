' All integers for speed
DEFINT A-Z

'$INCLUDE: 'DIRECTQB.BI'

' Allocates memory for a simple 32x32 pixels sprite
DIM Sprite(514)

' Let's initialize the library with one extra layer, no sounds nor free EMS
IF DQBinit(1, 0, 0) THEN DQBclose: PRINT DQBerror$: END

' Checks if a mouse is available
IF NOT DQBmouseDetected THEN
    ' Mouse not found!
    PRINT "This example requires a mouse to run!"
    DQBclose
    END
END IF

DQBinitVGA
DQBclearLayer 1

' Draws our sprite and gets it from a hidden layer
DQBline 1, 15, 0, 22, 26, 40
DQBline 1, 15, 0, 8, 26, 40
DQBline 1, 8, 26, 22, 26, 40
DQBpaint 1, 16, 16, 4
DQBget 1, 0, 0, 31, 31, VARSEG(Sprite(0)), VARPTR(Sprite(0))

' Starts demo
DQBresetMouse
DQBmouseShow
DO
  ' Clears layer 1 for double buffering effect
  DQBclearLayer 1
  
  ' Finds angle between the center of our sprite and the mouse cursor
  a = DQBangle(160, 100, DQBmouseX, DQBmouseY)

  ' Draws rotated sprite
  DQBrPut 1, 144, 84, VARSEG(Sprite(0)), VARPTR(Sprite(0)), a, 100

  ' Updates the screen
  DQBwait 1
  DQBmouseHide
  DQBcopyLayer 1, VIDEO
  DQBmouseShow

  ' Ends demo if the user presses a key or any of the mouse buttons
  IF DQBmouseLB OR DQBmouseRB OR INKEY$ <> "" THEN EXIT DO
LOOP

' Ends the program
DQBclose

