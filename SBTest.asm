' START OF RESET SECTION

OUT &H226, 1: OUT &H226, 0
DO
x% = INP(&H22E)
IF x% AND 128 THEN
        x% = INP(&H22A)
        IF x% = &HAA THEN
                PRINT "reset!"
                EXIT DO
        END IF
END IF
LOOP

' END OF RESET SECTION


' TURN SPEAKER ON
DO
x% = INP(&H22C)
LOOP WHILE x% AND 128
OUT &H22C, &HD1                                ' send speaker on code

DO
FOR g% = 100 TO 1 STEP -1                      ' START OF LOOP
OUT &H22C, &H10                                ' send
OUT &H22C, f%                                  ' send no.
FOR aa% = 1 TO 100: NEXT aa%                   ' delay
IF f% = 255 THEN f% = 0 ELSE f% = 255          ' alternate between 0 and 255
NEXT g%                                         ' END OF LOOP
LOOP

' Hope that's useful.


'--------------------------------------------------------

